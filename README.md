Project name: 
        
    CuSEr - Customer Smart Entertainer


Members:

	Gliga Mihai               (mihaigliga21@gmail.com)

	Hrisca Marius             (hrisca.marius@gmail.com)

	Visovan Iuliu             (iuliuvisovan@gmail.com)

Contact:

	Any of the e-mails above OR
	0754263001 (Iuliu Visovan)

URI:

	http://cuser.azurewebsites.net/ (soon)

Project presentation / blog:
	
	http://cuser.iuliu.net

Tags:

	cuser, customer smart entertainer, project, infoiasi, wade, web